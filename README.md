#classCompile

classCompile is a tool for compiling classes
if you have a class Dog you can put that in the compile function and it will compile it to whatever you want and pass all attributes of Dog to as example a Cat Object

##Documentation:
### compile:

>from compile.compileClasses import compileObject as comp
>comp(Dog(),typeCompilations=[(Dog, Cat())])

in typeCompilations are all things where you want to change the class

> typeCompilations=[
>   (oldObject, NewObject()),
>   (oldObject2, NewObject2())
>]