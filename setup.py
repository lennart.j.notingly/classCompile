from setuptools import setup, find_packages

setup(
    name='classCompile',
    version='1.0.0',
    description='its a compiler for python classes',
    author='lennart j',
    author_email='lennart.j@notingly.de',
    url='https://gitlab.com/lennart.j.notingly/classCompile',
    packages=find_packages(),
    install_requires=["dill"]
)
