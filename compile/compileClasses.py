from dill import copy


def compileObject(first_object, second_object = None, typeCompilations=[]):
    # first object will be compiled to second object
    # if first objects type is in the typeCompilation second object can be none None
    if type(first_object) is dict:  # if the fist object is a dictionary all attributes will pass to the second dict
        for key in first_object:
            value = first_object[key]
            try:  # the try catch is that if second object do not have th property it is gonna to be deleted
                second_object[key] = compileObject(value, second_object[key], typeCompilations)
            except KeyError:
                pass
        return second_object
    elif type(first_object) is list:  # check if the object is a list
        for i in range(len(first_object)):  # iterate through the list and compile objects in it
            first_object[i] = compileObject(first_object[i], typeCompilations=typeCompilations)
        return first_object
    else:
        for object_type, compile_object in typeCompilations:
            if type(first_object) is object_type:  # tests if object should be compiled to a other object type
                new_object = copy(compile_object)  # copys object so that no problems with pointers will come
                new_object.__dict__ = compileObject(first_object.__dict__, new_object.__dict__, typeCompilations)
                # with recoursion th dict of the object will pass to the compile function
                return new_object
        return first_object  # object will keep

