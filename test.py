from compile import compileClasses
import numpy as np

class CatHead():
    ears = "triangel"


class DogHead():
    ears = "round"


class Cat():
    def __init__(self, name="", age=0, fur="fluffy", get_older=lambda x: x + 2):
        self.name = name
        self.age = age
        self.fur = fur
        self.get_older=get_older
        self.head = np.array([CatHead(),CatHead(),CatHead()])

    def getOlder(self):
        self.age = self.get_older(self.age)

    def makeSound(self):
        print("woof")


class Dog():
    def __init__(self, name="", age=0, behauviour="wierd", get_older=lambda x: x + 1):
        self.name = name
        self.age = age
        self.behaviour = behauviour
        self.get_older = get_older
        self.head = np.array([DogHead(),DogHead(),CatHead()])

    def getOlder(self):
        self.age = self.get_older(self.age)

    def makeSound(self):
        print("miau")



pete = Dog()


new_pete = Cat()

print(compileClasses.compileObject(
    pete,
    None,
    [
        (Dog, Cat()),
        (DogHead, CatHead()),
        (CatHead, CatHead())
    ]
).head.dtype)

print(compileClasses.compileObject(
    {"age":14},
    {}
))